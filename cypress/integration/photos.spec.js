describe('Photos', () => {
    before(() => {
        cy.refreshDatabase();
    })

    it('shows a list of photos', () => {
        // Setup
        const photo = {
            'title': 'My first Photo',
            'photo': "https://picsum.photos/200/300",
            'is_public': false
        }
        const model = "App\\Models\\Photo"
        // Execute
        cy.create(model, photo)

        // assertion
        cy.visit('photos', {
            failOnStatusCode: false
        }).contains('My first Photo')

    })
    it('when we visit a single photo we see the photo')

})
