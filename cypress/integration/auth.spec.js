describe('Authentication', () => {
    before(() => {
        cy.refreshDatabase();
    })
    describe('Registration', () => {
        it('shows a registration link on the home page', () => {
            cy.visit('/').contains('Register')
        })
        it('shows a registration page with a form', () => {
            cy.get('a').contains('Register').click()
            cy.get('form').within(() => {
                cy.get('input#name')
                cy.get('input#email')
                cy.get('input#password')
                cy.get('input#password_confirmation')
            })

        })

        it('should be able to register an account', () => {
            cy.visit('/register')
            cy.get('form').within(() => {
                cy.get('input#name').type('Fabio{enter}')
                cy.get('input#email').type('fabio@example.com{enter}')
                cy.get('input#password').type('password{enter}')
                cy.get('input#password_confirmation').type('password')
                cy.get('button').contains('Register').click()
            })
            cy.get('body').contains('Dashboard')
            cy.get('body').contains('Fabio')
        })
    })

    describe('Login', () => {
        it('shows a login link on the home page', () => {
            cy.visit('/').contains('Log in')
        })
        it('should be able to login into an existing account', () => {
            cy.visit('/login')
            cy.get('form').within(() => {

                cy.get('input#email').type('fabio@example.com{enter}')
                cy.get('input#password').type('password')

                cy.get('button').contains('Log in').click()
            })
            cy.get('body').contains('Dashboard')
            cy.get('body').contains('Fabio')
        })
    })

})
